global class AnonymizationBatch implements Database.Batchable<sObject>, Database.Stateful, Schedulable {
    AnonymizationDataProvider dataProvider;
    AnonymizationExecutor executor;
    Integer iterator;
    List<String> queryList;

    global void execute(SchedulableContext sc) {
        Database.executeBatch(new AnonymizationBatch());
    }
    
    global AnonymizationBatch() {
        dataProvider = new AnonymizationDataProvider();
        executor = new AnonymizationExecutor();
        iterator = 0;
        queryList = dataProvider.getQueries();
    }

    global AnonymizationBatch(Integer i) {
        dataProvider = new AnonymizationDataProvider();
        executor = new AnonymizationExecutor();
        iterator = i;
        queryList = dataProvider.getQueries();
    }

    global Database.QueryLocator start(Database.BatchableContext bc) {
        // collect the batches of records or objects to be passed to execute
        String query = queryList.get(iterator);
        return Database.getQueryLocator(query);
    }

    global void execute(Database.BatchableContext bc, List<sObject> scope){
        // process each batch of records
        for(sObject obj : scope) {
            AnonymizationExecutor.executeAnonymizationProcess(obj.Id);
        }
    }

    global void finish(Database.BatchableContext bc){
        // execute any post-processing operations
        iterator++;
        if(iterator < queryList.size()) {
            Database.executeBatch(new AnonymizationBatch(iterator));
        }
    } 


}
