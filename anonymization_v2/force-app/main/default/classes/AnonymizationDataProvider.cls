public class AnonymizationDataProvider {

    List<GDPR_Data_To_Be_Anonymized__mdt> definitions;
    List<GDPR_Type_Anonymization_Pattern__mdt> fieldsHashPatterns;  
    List<GDPR_Object_Time_Period__mdt> objectsTimePeriods;

    public AnonymizationDataProvider() {
        definitions = AnonymizationDefinitionProvider.getDefinitions();
        fieldsHashPatterns = AnonymizationDefinitionProvider.getHashPatternForFields();
        objectsTimePeriods = AnonymizationDefinitionProvider.getTimePeriodsForObjects();
    }

    public List<String> getQueries() {
        Map<String, Set<String>> objectsFields = convertDefinintionsIntoMap();       
        List<String> queries = generateQuery(objectsFields);
        return queries;
    }

    public Map<String, Set<String>> convertDefinintionsIntoMap() {
        Map<String, Set<String>> definitionsMap = new Map<String, Set<String>>();
        for(GDPR_Data_To_Be_Anonymized__mdt def : definitions) {
                if (!definitionsMap.containsKey(def.Object_Name__c)) {
                    definitionsMap.put(def.Object_Name__c, new Set<String>());
                }
                definitionsMap.get(def.Object_Name__c).add(def.Field_Name__c);
        }
        return definitionsMap;
    }


    private List<String> generateQuery(Map<String, Set<String>> objectsFields) {
        List<String> queries = new List<String>();
        for (String obj : objectsFields.keySet()) {
            String query = 'SELECT ';
            query += String.join(new List<String>(objectsFields.get(obj)), ', ') + ' FROM ' + obj;
            if (getTimePeriodForObject(obj) != 0 && getTimeFieldNameForObject(obj) != '') {
                query += ' WHERE ' + getTimeFieldNameForObject(obj) + ' < LAST_N_DAYS:' + getTimePeriodForObject(obj);
            }
            // ADD CONDITIONS HERE
            // HOW TO LIMIT IT HERE
            queries.add(query);
        }
        return queries;
    }

    private Integer getTimePeriodForObject(String objName) {
        for (GDPR_Object_Time_Period__mdt obj : objectsTimePeriods) {
            if (obj.Object_Name__c==objName){
                return (Integer) obj.Time_Period__c; 
            }
        } 
        return 0;
    }

    private String getTimeFieldNameForObject(String objName) {
        for (GDPR_Object_Time_Period__mdt obj : objectsTimePeriods) {
            if (obj.Object_Name__c==objName){
                return obj.Date_Field_Name__c; 
            }
        } 
        return '';
    }

}
