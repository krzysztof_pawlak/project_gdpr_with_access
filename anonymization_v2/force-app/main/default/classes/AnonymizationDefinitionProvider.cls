public class AnonymizationDefinitionProvider {
    public static List<GDPR_Data_To_Be_Anonymized__mdt> getDefinitions() {
        return [SELECT Object_Name__c, Field_Name__c, Is_Active__c, Hash_Pattern__c  FROM GDPR_Data_To_Be_Anonymized__mdt WHERE Is_Active__c = true];
    }

    public static List<GDPR_Type_Anonymization_Pattern__mdt> getHashPatternForFields() {
        return [SELECT Field_Type__c, Hash_Pattern__c FROM GDPR_Type_Anonymization_Pattern__mdt];
    }

    public static List<GDPR_Object_Time_Period__mdt> getTimePeriodsForObjects() {
        return [SELECT Object_Name__c, Date_Field_Name__c, Time_Period__c FROM GDPR_Object_Time_Period__mdt];
    }
    public static List<GDPR_Object_Releated_Resource__mdt> getReleatedResourcesForObjects() {
        return [SELECT Object_Name__c, Related_Resource_Name__c FROM GDPR_Object_Releated_Resource__mdt];
    }
}
