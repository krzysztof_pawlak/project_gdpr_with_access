public class AnonymizationExecutor {
    
    AnonymizationDataProvider dataProvider;

    public AnonymizationExecutor() {
        dataProvider = new AnonymizationDataProvider();
    }

    //scrambling procedure
    @AuraEnabled
    public static void executeAnonymizationProcess(String myId) {

        System.debug('ID: ' + myId);
        AnonymizationDataProvider dataProvider = new AnonymizationDataProvider();
        Schema.SObjectType sobjectType = Id.valueOf(myId).getSObjectType();
        String sobjectName = sobjectType.getDescribe().getName();
        SObject objectToBeAnonymized = Database.query('SELECT Id, Name FROM ' + sobjectName + ' WHERE Id = :myId');

        List<GDPR_Data_To_Be_Anonymized__mdt> definitions = AnonymizationDefinitionProvider.getDefinitions();
        Map<String, Set<String>> objectFieldsMapCM = dataProvider.convertDefinintionsIntoMap();

        //for every field
        SObjectType objType = Schema.getGlobalDescribe().get(sobjectName);
        Map<String,Schema.SObjectField> fieldMap = objType.getDescribe().fields.getMap();    
        for(String fieldName : fieldMap.keySet()) { 
            //check if 1st CM contains
            if (objectFieldsMapCM.keySet().contains(sobjectName)){
                for(String fieldNameCM : objectFieldsMapCM.get(sobjectName)){
                    String temp = sobjectName + '.' + fieldName;
                    if (temp.toLowerCase() == fieldNameCM.toLowerCase()) {
                        objectToBeAnonymized.put(fieldName, 'XXXXX');
                        //here execute function for anonymizing with regex
                    }
                }
            }     
        }
        if(fieldMap.get('Is_Scrambled__c') != null){
            objectToBeAnonymized.put('Is_Scrambled__c',  true);
        }
        try {
            update objectToBeAnonymized;
        } catch(DmlException e) {
            System.debug('The following exception has occurred: ' + e.getMessage());
        }    
    }
}
