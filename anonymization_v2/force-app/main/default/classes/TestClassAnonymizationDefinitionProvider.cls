/**
* @author       Kacper Szymula
* @description  Test class for AnonymizationDefinitionProvider
**/

@isTest
private class TestClassAnonymizationDefinitionProvider {
    @isTest static void testMethodGetDefinitions() {
        System.assertNotEquals(AnonymizationDefinitionProvider.getDefinitions(), null);
    }
    @isTest static void testMethodGetHashPatternForFields() {
        System.assertNotEquals(AnonymizationDefinitionProvider.getHashPatternForFields(), null);
    }

    @isTest static void testMethodGetTimePeriodsForObjects() {
        System.assertNotEquals(AnonymizationDefinitionProvider.getTimePeriodsForObjects(), null);
    }

    @isTest static void TestMethodGetReleatedResourcesForObjects() {
        System.assertNotEquals(AnonymizationDefinitionProvider.getReleatedResourcesForObjects(), null);
    }
}
