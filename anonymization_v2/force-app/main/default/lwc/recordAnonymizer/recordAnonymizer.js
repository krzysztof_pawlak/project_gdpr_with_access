/* eslint-disable no-console */
/* eslint-disable no-alert */
/* eslint no-eval: "error" */
/* eslint-env es6 */

import { LightningElement, api } from 'lwc';
//import test from '@salesforce/apex/AnonymizationExecutor.test';
import executeAnonymizationProcess from '@salesforce/apex/AnonymizationExecutor.executeAnonymizationProcess';
import anonymize from '@salesforce/label/c.Anonymize';

export default class RecordAnonymizer extends LightningElement {
    @api recordId;
    label = {
        anonymize
    }

    async handleClick() {
        console.log('this.recordId: ' + this.recordId);
        await executeAnonymizationProcess({myId: this.recordId});
        // eslint-disable-next-line no-eval
        eval("$A.get('e.force:refreshView').fire();");
        console.log('this.recordId: ' + this.recordId);
    }
}